function calculafx_Secante(x, expressao) {
    return eval(expressao.replace("x", x)); //<================= COLOQUE A FUNCAO AQUI
}

function secante(expressao, a, b, x0, x1, PRECISAO) {


    var ValoresCalculo_a = [];
    var ValoresCalculo_b = [];
    var ValoresCalculo_Fx = [];
    var ValoresCalculo_Precisao = [];
    var ValoresCalculo_x0 = [];
    var ValoresCalculo_xn = [];
    var ValoresCalculo_x1 = [];


    /////////////////////////////   VARIAVEIS   //////////////////////////////
    var E = Math.pow(10, PRECISAO);
    var x;
    var xn = 0;
    var precisao;
    var interacao = 0;
    var fx;

    var fa = calculafx_Secante(a, expressao);
    var fb = calculafx_Secante(b, expressao);

    precisao = Math.abs(b - a);
    if (fa < 0 && fb > 0) {
        while (precisao >= E) {
            interacao++;
            xn = (x0 * calculafx_Secante(x1, expressao) - x1 * calculafx_Secante(x0, expressao)) / (calculafx_Secante(x1, expressao) - calculafx_Secante(x0, expressao));
            precisao = Math.abs(xn - x1);
            fx = calculafx_Secante(xn, expressao);
            if (fx < 0) {

                a = xn;
            } else {
                b = xn;
            }

            // printf("=================================================\n");
            // printf("interacao = %d \n", interacao);
            // printf("x%d = %.20lf \n", interacao - 1, x0);
            // printf("x%d = %.20lf \n", interacao, x1);
            // printf("x%d = %.20lf \n", interacao + 1, xn);
            // printf("f(x%d) = %.20lf \n", interacao + 1, fx);
            // printf("[%.20lf ; %.20lf] \n", a, b);
            // printf("|x%d-x%d|= %.20lf \n", interacao + 1, interacao, precisao);
            ValoresCalculo_a.push(a);
            ValoresCalculo_b.push(b);
            ValoresCalculo_Fx.push(fx);
            ValoresCalculo_Precisao.push(precisao);
            ValoresCalculo_x0.push(x0);
            ValoresCalculo_x1.push(x1);
            ValoresCalculo_xn.push(xn);


            x0 = x1;
            x1 = xn;



        }

    } else if (fa > 0 && fb < 0)
        while (precisao >= E) {
            interacao++;
            xn = (x0 * calculafx_Secante(x1, expressao) - x1 * calculafx_Secante(x0, expressao)) / (calculafx_Secante(x1, expressao) - calculafx_Secante(x0, expressao));
            precisao = Math.abs(xn - x1);
            fx = calculafx_Secante(xn, expressao);
            if (fx < 0) {

                b = xn;
            } else {
                a = xn;
            }

            // printf("=================================================\n");
            // printf("interacao = %d \n", interacao);
            // printf("x%d = %.20lf \n", interacao - 1, x0);
            // printf("x%d = %.20lf \n", interacao, x1);
            // printf("x%d = %.20lf \n", interacao + 1, xn);
            // printf("f(x%d) = %.20lf \n", interacao + 1, fx);
            // printf("[%.20lf ; %.20lf] \n", a, b);
            // printf("|x%d-x%d|= %.20lf \n", interacao + 1, interacao, precisao);
            x0 = x1;
            x1 = xn;
        }


    var ValoresCalculo = {
        ValoresCalculo_a: ValoresCalculo_a,
        ValoresCalculo_b: ValoresCalculo_b,
        ValoresCalculo_Fx: ValoresCalculo_Fx,
        ValoresCalculo_Precisao: ValoresCalculo_Precisao,
        ValoresCalculo_x: ValoresCalculo_x0,
        ValoresCalculo_x1: ValoresCalculo_x1,
        ValoresCalculo_xn: ValoresCalculo_xn
    }

    mostrarResultado(ValoresCalculo, 4);
}