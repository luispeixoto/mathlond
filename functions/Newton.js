function calculafx_Newton(x, expressao) {
    return eval(expressao.replace("x", x)); //<================= COLOQUE A FUNCAO AQUI
}

function calculaDerivada_Newton(x, expressao) {
    return eval(expressao.replace("x", x)); //<================= COLOQUE A FUNCAO DERIVADA AQUI
}


function calcula_Newton(x, expressao, ExpressaoDerivada) {
    return calculafx_Newton(x, expressao) / calculaDerivada_Newton(x, ExpressaoDerivada);
}



function Newton(expressao, ExpressaoDerivada, a, b, PontoIncial, PRECISAO) {

    var ValoresCalculo_a = [];
    var ValoresCalculo_b = [];
    var ValoresCalculo_Fx = [];
    var ValoresCalculo_Precisao = [];
    var ValoresCalculo_FxDerivada = [];
    var ValoresCalculo_x = [];
    var ValoresCalculo_xn = [];




    var E = Math.pow(10, PRECISAO); //<========================== PRECISAO
    ////INTERVALO [a,b]

    var x0 = PontoIncial; ////<================================== VALOR INICIAL

    /////////////////////////////   VARIAVEIS   //////////////////////////////
    var x;
    var xn = 0;
    var precisao;
    var interacao = 0;
    var fx;

    var fa = calculafx_Newton(a, expressao);
    var fb = calculafx_Newton(b, expressao);

    precisao = Math.abs(b - a);
    if (fa < 0 && fb > 0) {
        while (precisao >= E) {
            interacao++;
            xn = x0 - calcula_Newton(x0, expressao, ExpressaoDerivada);
            precisao = Math.abs(xn - x0);
            fx = calculafx_Newton(x0, expressao);
            if (fx < 0) {

                a = x0;
            } else {
                b = x0;
            }

            // printf("=================================================\n");
            // printf("interacao = %d \n", interacao);
            // printf("x%d = %.20lf \n", interacao - 1, x0);
            // printf("x%d = %.20lf \n", interacao, xn);
            // printf("f(x%d) = %.20lf \n", interacao, calculafx_Newton(x0));
            // printf("f'(x%d)a = %.20lf \n", interacao, calculaDerivada_Newton(x0));
            // printf("[%.20lf ; %.20lf] \n", a, b);
            // printf("|x%d-x%d|= %.20lf \n", interacao, interacao - 1, precisao);
            ValoresCalculo_a.push(a);
            ValoresCalculo_b.push(b);
            ValoresCalculo_Fx.push(calculafx_Newton(x0, expressao));
            ValoresCalculo_FxDerivada.push(calculaDerivada_Newton(x0, ExpressaoDerivada))
            ValoresCalculo_Precisao.push(precisao);
            ValoresCalculo_x.push(x0);
            ValoresCalculo_xn.push(xn);


            x0 = xn;



        }

    } else if (fa > 0 && fb < 0)
        while (precisao >= E) {
            interacao++;
            xn = x0 - calcula_Newton(x0, expressao, ExpressaoDerivada);
            precisao = Math.abs(xn - x0);
            fx = calculafx_Newton(x0, expressao);
            if (fx < 0) {

                b = x0;
            } else {
                a = x0;
            }

            // printf("=================================================\n");
            // printf("interacao = %d \n", interacao);
            // printf("x%d = %.20lf \n", interacao - 1, x0);
            // printf("x%d = %.20lf \n", interacao, xn);
            // printf("f(x%d) = %.20lf \n", interacao, calculafx(x0));
            // printf("f'(x%d)a = %.20lf \n", interacao, calculaDerivada(x0));
            // printf("[%.20lf ; %.20lf] \n", a, b);
            // printf("|x%d-x%d|= %.20lf \n", interacao, interacao - 1, precisao);
            ValoresCalculo_a.push(a);
            ValoresCalculo_b.push(b);
            ValoresCalculo_Fx.push(calculafx_Newton(x0, expressao));
            ValoresCalculo_FxDerivada.push(calculaDerivada_Newton(x0, ExpressaoDerivada))
            ValoresCalculo_Precisao.push(precisao);
            ValoresCalculo_x.push(x0);
            ValoresCalculo_xn.push(xn);

            x0 = xn;






        }


    var ValoresCalculo = {
        ValoresCalculo_a: ValoresCalculo_a,
        ValoresCalculo_b: ValoresCalculo_b,
        ValoresCalculo_Fx: ValoresCalculo_Fx,
        ValoresCalculo_Precisao: ValoresCalculo_Precisao,
        ValoresCalculo_FxDerivada: ValoresCalculo_FxDerivada,
        ValoresCalculo_x: ValoresCalculo_x,
        ValoresCalculo_xn: ValoresCalculo_xn
    }

    mostrarResultado(ValoresCalculo, 3);
}