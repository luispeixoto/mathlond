////////////////////////////////////// PONTO FIXO ///////////////////////////////
function calculafx_PontoFixo(x, expressao) {
    return eval(expressao.replace("x", x));
}


function calcula_PontoFixo(x, expressao) {
    return eval(expressao.replace("x", x)); //<================= COLOQUE A FUNCAO QUE SERA CALCULADA AQUI x=g(xk)
}


function PontoFixo(expressao, expressaoGX, PontoIncial, PRECISAO) {

    var ValoresCalculo_x = [];
    var ValoresCalculo_xn = [];
    var ValoresCalculo_Fx = [];
    var ValoresCalculo_Precisao = [];

    ///////////////////////////    VARIAVEIS   //////////////////////////////
    var xn = 0;
    var precisao;
    var interacao = 0;

    var x = PontoIncial; ////  COLOQUE O PONTO INICIAL
    var E = Math.pow(10, PRECISAO); ////  COLOQUE A PRECISAO

    precisao = Math.abs(xn - x);
    while (precisao >= E) {
        ValoresCalculo_x.push(x);
        interacao = interacao + 1;
        xn = calcula_PontoFixo(x, expressaoGX);
        ValoresCalculo_xn.push(xn)
        precisao = Math.abs(xn - x);
        ValoresCalculo_Precisao.push(precisao);
        ValoresCalculo_Fx.push(calculafx_PontoFixo(xn, expressao));

        //    printf("========================================================== \n");
        //    printf("interacao =%d  \n",interacao);
        //    printf("x%d =%.20lf \n",interacao-1,x);
        //    printf("x%d= %.20lf \n",interacao,xn);
        //    printf("f(x%d)= %.20lf \n",interacao,calculafx(xn));
        //    printf("|x%d-x%d|= %.20lf \n",interacao,interacao-1,precisao);
        x = xn;

    }

    var ValoresCalculo = {
        ValoresCalculo_a: ValoresCalculo_x,
        ValoresCalculo_b: ValoresCalculo_xn,
        ValoresCalculo_Fx: ValoresCalculo_Fx,
        ValoresCalculo_Precisao: ValoresCalculo_Precisao
    }

    mostrarResultado(ValoresCalculo, 2);

}