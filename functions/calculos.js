var ListResultado = document.querySelector('#tabela');
var FormCalculos = document.querySelector('#InfoCalculo');



var e = 2.718281828459045235360287;



// var InputCalculoElement = document.getElementById('InputCalculo');

var BTcalculoElement_Bissecao = document.querySelector('button#Bissecao');
var BTcalculoElement_PontoFixo = document.querySelector('button#PontoFixo');
var BTcalculoElement_Newton = document.querySelector('button#Newton');
var BTcalculoElement_Secante = document.querySelector('button#Secante');




BTcalculoElement_Bissecao.onclick = function() {



    ListResultado.innerHTML = '';


    FormCalculos.innerHTML = '';

    FormCalculos.setAttribute("class", "InfoCalculo");



    var InputExpressao = document.createElement('input');
    InputExpressao.setAttribute("id", "Input_BissecaoExpressao");
    InputExpressao.setAttribute("class", "InputForm");
    InputExpressao.setAttribute("placeholder", "Expressão f(x)");




    var InputA = document.createElement('input');
    InputA.setAttribute("id", "Input_BissecaoA");
    InputA.setAttribute("class", "InputForm");
    InputA.setAttribute("placeholder", "Valor de A");


    var InputB = document.createElement('input');
    InputB.setAttribute("id", "Input_BissecaoB");
    InputB.setAttribute("class", "InputForm");
    InputB.setAttribute("placeholder", "Valor de B");



    var Input_Precisao = document.createElement('input');
    Input_Precisao.setAttribute("id", "Input_BissecaoPrecisao");
    Input_Precisao.setAttribute("class", "InputForm");
    Input_Precisao.setAttribute("placeholder", "Precisão Ex:(-3)");




    var BTCalculo = document.createElement('button');
    BTCalculo.setAttribute("class", "BTcalculo");
    BTCalculo.setAttribute("id", "BTcalculo_Calcular");
    textButton = document.createTextNode('Calcular');
    BTCalculo.appendChild(textButton);




    FormCalculos.appendChild(InputExpressao);

    FormCalculos.appendChild(InputA);

    FormCalculos.appendChild(InputB);

    FormCalculos.appendChild(Input_Precisao);
    FormCalculos.appendChild(BTCalculo);




    var InputExpressaoElement = document.getElementById('Input_BissecaoExpressao');
    var InputAElement = document.getElementById('Input_BissecaoA');
    var InputBElement = document.getElementById('Input_BissecaoB');
    var Input_PrecisaoElement = document.getElementById('Input_BissecaoPrecisao');

    var button_calculo = document.querySelector('button#BTcalculo_Calcular');


    button_calculo.onclick = function() {
        var expressao = InputExpressaoElement.value;
        var a = InputAElement.value;
        var b = InputBElement.value;
        var precisao = Input_PrecisaoElement.value;




        Bissecao(expressao, eval(a), eval(b), eval(precisao));

    }


}




BTcalculoElement_PontoFixo.onclick = function() {

    ListResultado.innerHTML = '';



    FormCalculos.innerHTML = '';


    FormCalculos.setAttribute("class", "InfoCalculo");

    var InputExpressao = document.createElement('input');
    InputExpressao.setAttribute("id", "Input_PontoFixoExpressao");
    InputExpressao.setAttribute("class", "InputForm");
    InputExpressao.setAttribute("placeholder", "Expressão f(x)");



    var InputExpressaoGX = document.createElement('input');
    InputExpressaoGX.setAttribute("id", "Input_PontoFixoExpressaoGX");
    InputExpressaoGX.setAttribute("class", "InputForm");
    InputExpressaoGX.setAttribute("placeholder", "Expressão g(x)");


    var InputPontoInicial = document.createElement('input');
    InputPontoInicial.setAttribute("id", "Input_PontoFixoPontoInicial");
    InputPontoInicial.setAttribute("class", "InputForm");
    InputPontoInicial.setAttribute("placeholder", "Ponto inicial (x0)");



    var Input_Precisao = document.createElement('input');
    Input_Precisao.setAttribute("id", "Input_PontoFixoPrecisao");
    Input_Precisao.setAttribute("class", "InputForm");
    Input_Precisao.setAttribute("placeholder", "Precisão Ex:(-3)");



    var BTCalculo = document.createElement('button');
    BTCalculo.setAttribute("class", "BTcalculo");
    BTCalculo.setAttribute("id", "BTcalculo_Calcular");
    textButton = document.createTextNode('Calcular');
    BTCalculo.appendChild(textButton);



    FormCalculos.appendChild(InputExpressao);

    FormCalculos.appendChild(InputExpressaoGX);

    FormCalculos.appendChild(InputPontoInicial);

    FormCalculos.appendChild(Input_Precisao);
    FormCalculos.appendChild(BTCalculo);



    var InputExpressaoElement = document.getElementById('Input_PontoFixoExpressao');
    var InputExpressaoGXElement = document.getElementById('Input_PontoFixoExpressaoGX');
    var InputPontoInicialElement = document.getElementById('Input_PontoFixoPontoInicial');
    var Input_PrecisaoElement = document.getElementById('Input_PontoFixoPrecisao');

    var button_calculo = document.querySelector('button#BTcalculo_Calcular');

    button_calculo.onclick = function() {
        var expressao = InputExpressaoElement.value;
        var expressaoGX = InputExpressaoGXElement.value;
        var PontoIncial = InputPontoInicialElement.value;
        var precisao = Input_PrecisaoElement.value;




        PontoFixo(expressao, expressaoGX, eval(PontoIncial), eval(precisao));

    }


}



BTcalculoElement_Newton.onclick = function() {

    ListResultado.innerHTML = '';



    FormCalculos.innerHTML = '';


    FormCalculos.setAttribute("class", "InfoCalculo");


    var InputExpressao = document.createElement('input');
    InputExpressao.setAttribute("id", "Input_NewtonExpressao");
    InputExpressao.setAttribute("class", "InputForm");
    InputExpressao.setAttribute("placeholder", "Expressão f(x)");


    var InputExpressaoDerivada = document.createElement('input');
    InputExpressaoDerivada.setAttribute("id", "Input_NewtonExpressaoDerivada");
    InputExpressaoDerivada.setAttribute("class", "InputForm");
    InputExpressaoDerivada.setAttribute("placeholder", "Expressão derivada f'(x)");


    var InputA = document.createElement('input');
    InputA.setAttribute("id", "Input_NewtonA");
    InputA.setAttribute("class", "InputForm");
    InputA.setAttribute("placeholder", "Valor de A");


    var InputB = document.createElement('input');
    InputB.setAttribute("id", "Input_NewtonB");
    InputB.setAttribute("class", "InputForm");
    InputB.setAttribute("placeholder", "Valor de B");


    var InputPontoInicial = document.createElement('input');
    InputPontoInicial.setAttribute("id", "Input_NewtonPontoInicial");
    InputPontoInicial.setAttribute("class", "InputForm");
    InputPontoInicial.setAttribute("placeholder", "Ponto inicial (x0)");



    var Input_Precisao = document.createElement('input');
    Input_Precisao.setAttribute("id", "Input_NewtonPrecisao");
    Input_Precisao.setAttribute("class", "InputForm");
    Input_Precisao.setAttribute("placeholder", "Precisão Ex:(-3)");



    var BTCalculo = document.createElement('button');
    BTCalculo.setAttribute("class", "BTcalculo");
    BTCalculo.setAttribute("id", "BTcalculo_Calcular");
    textButton = document.createTextNode('Calcular');
    BTCalculo.appendChild(textButton);




    FormCalculos.appendChild(InputExpressao);

    FormCalculos.appendChild(InputExpressaoDerivada);

    FormCalculos.appendChild(InputA);

    FormCalculos.appendChild(InputB);

    FormCalculos.appendChild(InputPontoInicial);

    FormCalculos.appendChild(Input_Precisao);
    FormCalculos.appendChild(BTCalculo);



    var InputExpressaoElement = document.getElementById('Input_NewtonExpressao');
    var InputExpressaoDerivadaElement = document.getElementById('Input_NewtonExpressaoDerivada');
    var InputAElement = document.getElementById('Input_NewtonA');
    var InputBElement = document.getElementById('Input_NewtonB');
    var InputPontoInicialElement = document.getElementById('Input_NewtonPontoInicial');
    var Input_PrecisaoElement = document.getElementById('Input_NewtonPrecisao');

    var button_calculo = document.querySelector('button#BTcalculo_Calcular');

    button_calculo.onclick = function() {
        var expressao = InputExpressaoElement.value;
        var ExpressaoDerivada = InputExpressaoDerivadaElement.value;
        var a = InputAElement.value;
        var b = InputBElement.value;
        var PontoIncial = InputPontoInicialElement.value;
        var precisao = Input_PrecisaoElement.value;




        Newton(expressao, ExpressaoDerivada, eval(a), eval(b), eval(PontoIncial), eval(precisao));

    }
}

BTcalculoElement_Secante.onclick = function() {

    ListResultado.innerHTML = '';



    FormCalculos.innerHTML = '';

    FormCalculos.setAttribute("class", "InfoCalculo");



    var InputExpressao = document.createElement('input');
    InputExpressao.setAttribute("id", "Input_SecanteExpressao");
    InputExpressao.setAttribute("class", "InputForm");
    InputExpressao.setAttribute("placeholder", "Expressão f(x)");

    var InputA = document.createElement('input');
    InputA.setAttribute("id", "Input_SecanteA");
    InputA.setAttribute("class", "InputForm");
    InputA.setAttribute("placeholder", "Valor de A");




    var InputB = document.createElement('input');
    InputB.setAttribute("id", "Input_SecanteB");
    InputB.setAttribute("class", "InputForm");
    InputB.setAttribute("placeholder", "Valor de B");


    var InputPontoInicial = document.createElement('input');
    InputPontoInicial.setAttribute("id", "Input_SecantePontoInicial");
    InputPontoInicial.setAttribute("class", "InputForm");
    InputPontoInicial.setAttribute("placeholder", "Ponto inicial (x0)");



    var InputPontoSecundario = document.createElement('input');
    InputPontoSecundario.setAttribute("id", "Input_SecantePontoSecundario");
    InputPontoSecundario.setAttribute("class", "InputForm");
    InputPontoSecundario.setAttribute("placeholder", "Ponto secundario (x1)");



    var Input_Precisao = document.createElement('input');
    Input_Precisao.setAttribute("id", "Input_SecantePrecisao");
    Input_Precisao.setAttribute("class", "InputForm");
    Input_Precisao.setAttribute("placeholder", "Precisão Ex:(-3)");


    var BTCalculo = document.createElement('button');
    BTCalculo.setAttribute("class", "BTcalculo");
    BTCalculo.setAttribute("id", "BTcalculo_Calcular");
    textButton = document.createTextNode('Calcular');
    BTCalculo.appendChild(textButton);



    FormCalculos.appendChild(InputExpressao);

    FormCalculos.appendChild(InputA);

    FormCalculos.appendChild(InputB);

    FormCalculos.appendChild(InputPontoInicial);

    FormCalculos.appendChild(InputPontoSecundario);

    FormCalculos.appendChild(Input_Precisao);

    FormCalculos.appendChild(BTCalculo);



    var InputExpressaoElement = document.getElementById('Input_SecanteExpressao');
    var InputAElement = document.getElementById('Input_SecanteA');
    var InputBElement = document.getElementById('Input_SecanteB');
    var InputPontoInicialElement = document.getElementById('Input_SecantePontoInicial');
    var InputPontoSecundarioElement = document.getElementById('Input_SecantePontoSecundario');
    var Input_PrecisaoElement = document.getElementById('Input_SecantePrecisao');

    var button_calculo = document.querySelector('button#BTcalculo_Calcular');

    button_calculo.onclick = function() {
        var expressao = InputExpressaoElement.value;
        var a = InputAElement.value;
        var b = InputBElement.value;
        var PontoIncial = InputPontoInicialElement.value;
        var PontoSecundario = InputPontoSecundarioElement.value;
        var precisao = Input_PrecisaoElement.value;




        secante(expressao, eval(a), eval(b), eval(PontoIncial), eval(PontoSecundario), eval(precisao));

    }
}



function mostrarResultado(ValoresCalculo, type) {

    ListResultado.innerHTML = '';

    FormCalculos.innerHTML = '';





    FormCalculos.removeAttribute("class");

    var Tabela = document.createElement('table');
    Tabela.setAttribute("class", "zui-table");
    var tableElement = document.createElement("tr");
    var thead = document.createElement('thead');


    var linhasInfo = document.createElement('th');
    var linhasInfo_Text = document.createTextNode('Interacao');
    linhasInfo.appendChild(linhasInfo_Text);


    tableElement.appendChild(linhasInfo);

    thead.appendChild(tableElement);
    Tabela.appendChild(thead);


    if (type == 1 || type == 3 || type == 4) {




        var linhasInfo = document.createElement('th');
        var linhasInfo_Text = document.createTextNode('a');
        linhasInfo.appendChild(linhasInfo_Text);



        tableElement.appendChild(linhasInfo);

        thead.appendChild(tableElement);
        Tabela.appendChild(thead);

        ///////////////////////////////////////////////////////////

        var linhasInfo = document.createElement('th');
        var linhasInfo_Text = document.createTextNode('b');
        linhasInfo.appendChild(linhasInfo_Text);



        tableElement.appendChild(linhasInfo);

        thead.appendChild(tableElement);
        Tabela.appendChild(thead);



    }

    if (type != 2) {
        var linhasInfo = document.createElement('th');
        var linhasInfo_Text = document.createTextNode('Fx');
        linhasInfo.appendChild(linhasInfo_Text);

        tableElement.appendChild(linhasInfo);

        thead.appendChild(tableElement);
        Tabela.appendChild(thead);
    }
    if (type == 2 || type == 3 || type == 4) {





        var linhasInfo = document.createElement('th');
        var linhasInfo_Text = document.createTextNode('Xk');
        linhasInfo.appendChild(linhasInfo_Text);



        tableElement.appendChild(linhasInfo);

        thead.appendChild(tableElement);
        Tabela.appendChild(thead);

        ///////////////////////////////////////////////////////////

        var linhasInfo = document.createElement('th');
        var linhasInfo_Text = document.createTextNode('X(k+1)');
        linhasInfo.appendChild(linhasInfo_Text);



        tableElement.appendChild(linhasInfo);

        thead.appendChild(tableElement);
        Tabela.appendChild(thead);

        if (type == 4) {
            var linhasInfo = document.createElement('th');
            var linhasInfo_Text = document.createTextNode('X(k+2)');
            linhasInfo.appendChild(linhasInfo_Text);



            tableElement.appendChild(linhasInfo);

            thead.appendChild(tableElement);
            Tabela.appendChild(thead);
        }


    }

    if (type == 2) {
        var linhasInfo = document.createElement('th');
        var linhasInfo_Text = document.createTextNode('Fx');
        linhasInfo.appendChild(linhasInfo_Text);

        tableElement.appendChild(linhasInfo);

        thead.appendChild(tableElement);
        Tabela.appendChild(thead);
    }
    //////////////////////////////////////////////////////////



    if (type == 3) {
        var linhasInfo = document.createElement('th');
        var linhasInfo_Text = document.createTextNode("Fx'");
        linhasInfo.appendChild(linhasInfo_Text);

    }

    tableElement.appendChild(linhasInfo);

    thead.appendChild(tableElement);
    Tabela.appendChild(thead);

    //////////////////////////////////////////////////////////

    var linhasInfo = document.createElement('th');
    var linhasInfo_Text = document.createTextNode('Precisao');
    linhasInfo.appendChild(linhasInfo_Text);



    tableElement.appendChild(linhasInfo);

    thead.appendChild(tableElement);
    Tabela.appendChild(thead);




    var tbody = document.createElement('tbody');
    for (var i = 0; i < ValoresCalculo.ValoresCalculo_a.length; i++) {
        var tableElement = document.createElement("tr");


        var colunas = document.createElement('td');
        var Text = document.createTextNode(i);
        colunas.appendChild(Text);



        tableElement.appendChild(colunas);
        tbody.appendChild(tableElement)
        Tabela.appendChild(tbody);


        var colunas = document.createElement('td');
        var Text = document.createTextNode(ValoresCalculo.ValoresCalculo_a[i]);
        colunas.appendChild(Text);



        tableElement.appendChild(colunas);
        tbody.appendChild(tableElement)
        Tabela.appendChild(tbody);


        var colunas = document.createElement('td');
        var Text = document.createTextNode(ValoresCalculo.ValoresCalculo_b[i]);
        colunas.appendChild(Text);



        tableElement.appendChild(colunas);
        tbody.appendChild(tableElement)
        Tabela.appendChild(tbody);


        var colunas = document.createElement('td');
        var Text = document.createTextNode(ValoresCalculo.ValoresCalculo_Fx[i]);
        colunas.appendChild(Text);



        tableElement.appendChild(colunas);
        tbody.appendChild(tableElement)
        Tabela.appendChild(tbody);


        if (type == 3 || type == 4) {
            var colunas = document.createElement('td');
            var Text = document.createTextNode(ValoresCalculo.ValoresCalculo_x[i]);
            colunas.appendChild(Text);



            tableElement.appendChild(colunas);
            tbody.appendChild(tableElement)
            Tabela.appendChild(tbody);

            if (type == 4) {
                var colunas = document.createElement('td');
                var Text = document.createTextNode(ValoresCalculo.ValoresCalculo_x1[i]);
                colunas.appendChild(Text);



                tableElement.appendChild(colunas);
                tbody.appendChild(tableElement)
                Tabela.appendChild(tbody);
            }



            var colunas = document.createElement('td');
            var Text = document.createTextNode(ValoresCalculo.ValoresCalculo_xn[i]);
            colunas.appendChild(Text);



            tableElement.appendChild(colunas);
            tbody.appendChild(tableElement)
            Tabela.appendChild(tbody);

            if (type == 3) {

                var colunas = document.createElement('td');
                var Text = document.createTextNode(ValoresCalculo.ValoresCalculo_FxDerivada[i]);
                colunas.appendChild(Text);



                tableElement.appendChild(colunas);
                tbody.appendChild(tableElement)
                Tabela.appendChild(tbody);
            }
        }

        var colunas = document.createElement('td');
        var Text = document.createTextNode(ValoresCalculo.ValoresCalculo_Precisao[i]);
        colunas.appendChild(Text);



        tableElement.appendChild(colunas);
        tbody.appendChild(tableElement)
        Tabela.appendChild(tbody);

        ListResultado.appendChild(Tabela);





    }

    console.log(Tabela);

}


// Bissecao();


// Newton();
// PontoFixo();