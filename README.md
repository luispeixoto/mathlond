# MathLond
![](https://i.ibb.co/KqNCRGy/MathLond.jpg)

Site com objetivo de realizar cálculos numéricos =>https://luispeixoto.github.io/MathLond/

## Como usar:
**Obs**: No campo do F(x) é necessário utilizar o formato de objeto **Math** do JavaScript,
para saber mais [clique aqui](http://https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Math "clique aqui").



Selecionando a função de **Bissecao**, o primeiro campo é colocado F(x), os dois campos seguintes o de **a** e **b** são os intervalos do formato [a,b] e o ultimo campo é posto a precisão.

**Veja o exemplo abaixo:**

![](https://i.ibb.co/YQrn7sF/gif.gif)

![](https://i.ibb.co/m4Vp1nC/gif-1.gif)

![](https://i.ibb.co/Dg0VrPS/gif-2.gif)

**Passando para  MathLond:**
![](https://i.ibb.co/mvs7bH4/2.png)

**Resultado:**
![](https://i.ibb.co/W6LfKyt/3.png)


### Bissecao
A função se refere ao Método de Bisseção.

### Pontofixo
A função se refere ao Método do Ponto fixo.

### Newton
A função se refere ao Método de Newton–Raphson.

### Secantes
A função se refere ao Método das secantes.
